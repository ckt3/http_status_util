## Table of contents
* [Info](#info)
* [Dependencies](#dependencies)
* [Assumptions](#assumptions)
* [Usage](#usage)
* [Invocation](#invocation)

## Info
This is simple Python utility to calculate the percentage of HTTP 500 response codes
for one or more HTTP logs files during a given time interval. 

## Dependencies
There are no external dependencies. The only modules in use are from the Python 3.7.5
standard library.

## Assumptions
We assume that log file(s) input to this utility have pipe("|") delimited fields with the
following format:
```bash
1493969101.644 | https | vimeo.com | GET | 404 | 1493969101583195,1493969101635320, | IAD |  | 10.10.3.59 | 0.009
```
## Usage
```bash
./http_status_util.py --help
usage: http_status_util.py [-h] -f FILES [FILES ...] -s START -e END [-k KIND]

A utility to parse and aggregate a specific HTTP response code by domain
during a given time interval. The utility returns the percent of the total
that the HTTP response code comprises. The utility searches for a HTTP 500
status code by default however a different HTTP status can be specified by
overriding this default behavior with a flag.

optional arguments:
  -h, --help            show this help message and exit
  -f FILES [FILES ...], --files FILES [FILES ...]
                        One or more log files to to process. If more than one
                        file needs to processed filename args should be
                        separated by whitespace ex: --files file1 file2 file3
  -s START, --start START
                        start time, The timestamp from which to begin,
                        specified in a format of either: "%Y-%m-%d %H:%M:%S"
                        or "%Y-%m-%d". The start time is inclusive. UTC is the
                        only timezone supported and will be used automatically
  -e END, --end END     end time, The upperbound of the time interval of which
                        we will consider, specified in format of either:
                        "%Y-%m-%d %H:%M:%S", or "%Y-%m-%d". End time is
                        exclusive. UTC is the only timezone supported and will
                        be used automatically
  -k KIND, --kind KIND  Specify any non-500 HTTP status code to check. This
                        utility defaults to an HTTP 500 status code unless
                        overridden with this flag.
```

## Invocation
```bash
cat << EOF > 66-percent-500s.log
1493969101.644 | https | vimeo.com | GET | 200 | 1493969101583195,1493969101635320, | IAD |  | 10.10.3.59 | 0.009
1493969101.644 | https | foo.vimeo.com | GET | 200 | 1493969101583195,1493969101635320, | IAD |  | 10.10.3.59 | 0.009
1493969101.644 | https | bar.vimeo.com | GET | 200 | 1493969101583195,1493969101635320, | IAD |  | 10.10.3.59 | 0.009
1493969102.644 | https | vimeo.com | GET | 500 | 1493969101583195,1493969101635320, | IAD |  | 10.10.3.59 | 0.009
1493969102.644 | https | foo.vimeo.com | GET | 500 | 1493969101583195,1493969101635320, | IAD |  | 10.10.3.59 | 0.009
1493969102.644 | https | bar.vimeo.com | GET | 500 | 1493969101583195,1493969101635320, | IAD |  | 10.10.3.59 | 0.009
1493969101.644 | https | vimeo.com | GET | 500 | 1493969101583195,1493969101635320, | IAD |  | 10.10.3.59 | 0.009
1493969101.644 | https | foo.vimeo.com | GET | 500 | 1493969101583195,1493969101635320, | IAD |  | 10.10.3.59 | 0.009
1493969101.644 | https | bar.vimeo.com | GET | 500 | 1493969101583195,1493969101635320, | IAD |  | 10.10.3.59 | 0.009
EOF
```

Running either of the following queries:
```bash
./http_status_util.py --files  66-percent-500s.log --start 2017-05-05 --end 2017-05-06 
```
or
```bash
./http_status_util.py --files  66-percent-500s.log --start 2017-05-05T:07:25:00 --end 2017-05-06T:07:25:02 
```

returns:
```bash
Between time 2017-05-05 07:25:00+00:00 and time 2017-05-06 07:25:02+00:00:
vimeo.com returned 66.67% 500 errors
foo.vimeo.com returned 66.67% 500 errors
bar.vimeo.com returned 66.67% 500 errors
```
___

```bash
cat << EOF > 50-percent-500s-1.log
1493969101.644 | https | vimeo.com | GET | 200 | 1493969101583195,1493969101635320, | IAD |  | 10.10.3.59 | 0.009
1493969101.644 | https | foo.vimeo.com | GET | 200 | 1493969101583195,1493969101635320, | IAD |  | 10.10.3.59 | 0.009
1493969101.644 | https | bar.vimeo.com | GET | 200 | 1493969101583195,1493969101635320, | IAD |  | 10.10.3.59 | 0.009
1493969102.644 | https | vimeo.com | GET | 500 | 1493969101583195,1493969101635320, | IAD |  | 10.10.3.59 | 0.009
1493969102.644 | https | foo.vimeo.com | GET | 500 | 1493969101583195,1493969101635320, | IAD |  | 10.10.3.59 | 0.009
1493969102.644 | https | bar.vimeo.com | GET | 500 | 1493969101583195,1493969101635320, | IAD |  | 10.10.3.59 | 0.009
EOF
```

```bash
cat << EOF > 50-percent-500s-2.log
1493969101.644 | https | vimeo.com | GET | 200 | 1493969101583195,1493969101635320, | IAD |  | 10.10.3.59 | 0.009
1493969101.644 | https | foo.vimeo.com | GET | 200 | 1493969101583195,1493969101635320, | IAD |  | 10.10.3.59 | 0.009
1493969101.644 | https | bar.vimeo.com | GET | 200 | 1493969101583195,1493969101635320, | IAD |  | 10.10.3.59 | 0.009
1493969102.644 | https | vimeo.com | GET | 500 | 1493969101583195,1493969101635320, | IAD |  | 10.10.3.59 | 0.009
1493969102.644 | https | foo.vimeo.com | GET | 500 | 1493969101583195,1493969101635320, | IAD |  | 10.10.3.59 | 0.009
1493969102.644 | https | bar.vimeo.com | GET | 500 | 1493969101583195,1493969101635320, | IAD |  | 10.10.3.59 | 0.009
EOF
```
Running either of the following queries:
```bash
./http_status_util.py --files  50-percent-500s-1.log 50-percent-500s-2.log --start 2017-05-05 --end 2017-05-06
```
or
```bash
./http_status_util.py --files  50-percent-500s-1.log 50-percent-500s-2.log --start 2017-05-05T:07:25:00 --end 2017-05-06T:07:25:02
```

returns:
```bash
Between time 2017-05-05 00:00:00+00:00 and time 2017-05-06 00:00:00+00:00:
vimeo.com returned 50.00% 500 errors
foo.vimeo.com returned 50.00% 500 errors
bar.vimeo.com returned 50.00% 500 errors
```
___
```bash
cat << EOF > no-500s.log
1493969101.644 | https | vimeo.com | GET | 200 | 1493969101583195,1493969101635320, | IAD |  | 10.10.3.59 | 0.009
1493969101.644 | https | foo.vimeo.com | GET | 200 | 1493969101583195,1493969101635320, | IAD |  | 10.10.3.59 | 0.009
1493969101.644 | https | bar.vimeo.com | GET | 200 | 1493969101583195,1493969101635320, | IAD |  | 10.10.3.59 | 0.009
1493969102.644 | https | vimeo.com | GET | 200 | 1493969101583195,1493969101635320, | IAD |  | 10.10.3.59 | 0.009
1493969102.644 | https | foo.vimeo.com | GET | 200 | 1493969101583195,1493969101635320, | IAD |  | 10.10.3.59 | 0.009
1493969102.644 | https | bar.vimeo.com | GET | 200 | 1493969101583195,1493969101635320, | IAD |  | 10.10.3.59 | 0.009
EOF
```
Running either of the following queries:
```bash
./http_status_util.py --files  no-500s.log --start 2017-05-05 --end 2017-05-06
```
or
```bash
./http_status_util.py --files  no-500s.log  --start 2017-05-05T:07:25:00 --end 2017-05-06T:07:25:02
```

returns:
```bash
Between time 2017-05-05 00:00:00+00:00 and time 2017-05-06 00:00:00+00:00:
vimeo.com returned 0.00% 500 errors
foo.vimeo.com returned 0.00% 500 errors
bar.vimeo.com returned 0.00% 500 errors
```
___

query specifying a time interval not included in any log file(s) that were input:

Running:
```bash
./http_status_util.py --files  50-percent-500s-1.log 50-percent-500s-2.log --start 2017-05-06 --end 2017-05-07
```
or
```bash
./http_status_util.py --files  50-percent-500s-1.log 50-percent-500s-2.log --start 2017-05-05T:07:25:10 --end 2017-05-06T:07:25:11 
```
returns:
```bash
There were no log entries found for the time interval specified.
```
