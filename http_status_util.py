#!/usr/bin/env python3

import argparse
import re
from datetime import datetime, timezone


def validate_date_string(date_string):
    '''
    Function to validate that the datetime format
    passed in is one of the two ISO8601 formats
    this utility supports. It also validates that
    the date and times are compliant for that format.

   Args:
        date_string (string): Should be of the format
         %Y-%m-%d or %Y-%m-%dT:%H:%M:%S

    Returns:
        A Python datetime object with an UTC timezone.

    Raises:
        ArgumentTypeError: If either the start or end time input
        strings could not be parsed successfully.

    '''
    date_format1 = "^\d\d\d\d-\d\d-\d\d$"
    regex1 = re.compile(date_format1)

    date_format2 = "^\d\d\d\d-\d\d-\d\dT:\d\d:\d\d\:\d\d$"
    regex2 = re.compile(date_format2)

    if regex1.match(date_string):
        try:
            datetime_obj = datetime.strptime(date_string,
                                             "%Y-%m-%d")
        except ValueError:
            raise argparse.ArgumentTypeError(" Unsupported date format: {},"
                                             " format should be of the format"
                                             " %Y-%m-%d".format(date_string))
        datetime_obj = datetime_obj.replace(tzinfo=timezone.utc)
        return datetime_obj

    elif regex2.match(date_string):
        try:
            datetime_obj = datetime.strptime(date_string,
                                             "%Y-%m-%dT:%H:%M:%S")
        except ValueError:
            raise argparse.ArgumentTypeError("Unsupported date format: {},"
                                             " format should be of the format"
                                             " %Y-%m-%dT:%H:%M:%S"
                                             .format(date_string))
        datetime_obj = datetime_obj.replace(tzinfo=timezone.utc)
        return datetime_obj
    else:
        raise argparse.ArgumentTypeError("Unsupported date format: {},"
                                         " format should be one of the"
                                         " following supported ISO8601"
                                         " formats: %Y-%m-%d or"
                                         " %Y-%m-%dT:%H:%M:%S"
                                         .format(date_string))


def parse_log(files, start, end, kind=500):
    '''
    Function to parse HTTP logs and display the
    percentag of those HTTP response codes that
    were returned within the specified time range.

    Args:
        files (list): A list of log files to check.

        start (Python datetime object): Start time
        from which to begin checking. The start time
        is inclusive.

        end (Python datetime object): End time, the
        distance from the start for which we are
        interested in checking. The end time is exclusive.

        kind(int, optional): The kind of HTTP response code
        we are interested in. The is a means of overriding the
        500 HTTP status code to which this utility defaults to.
    '''
    log_stats = dict()
    for file in files:
        with open(file) as fh:
            for line in fh:
                timestamp, _1, domain, _3, status = line.split("|")[0:5]
                domain = domain.strip()
                ts = datetime.fromtimestamp(float(timestamp), tz=timezone.utc)
                if start <= ts < end:
                    if domain not in log_stats:
                        log_stats[domain] = dict()
                        log_stats[domain] = {"status_count": 0,
                                             "total_count": 0}
                    if kind == int(status):
                        log_stats[domain]["status_count"] += 1
                        log_stats[domain]["total_count"] += 1
                    else:
                        log_stats[domain]["total_count"] += 1

    if not log_stats:
        print("There were no log entries found for the time interval"
              " specified.")
        return
    if str(kind).startswith(str(5)) or str(kind).startswith(str(4)):
        word = "errors"
    else:
        word = "responses"
    print("Between time {} and time {}:".format(start, end))
    for domain in log_stats.keys():
        print("{} returned {:1.2f}% {} {}"
              .format(domain,
                      (log_stats[domain]["status_count"]
                       / log_stats[domain]["total_count"])
                      * 100, kind, word))


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="A utility to parse and"
                                     " aggregate a specific HTTP"
                                     " response code by domain during a"
                                     " given time interval. The utility"
                                     " returns the percent of the total"
                                     " that the HTTP response code comprises."
                                     " The utility searches for a HTTP 500"
                                     " status code by default however a"
                                     " different HTTP status can be specified"
                                     " by overriding this default behavior"
                                     " with a flag")

    parser.add_argument("-f", "--files",
                        nargs='+',
                        required=True,
                        help="One or more log files to"
                        " to process. If more than one file"
                        " needs to processed filename args should"
                        " be separated by whitespace ex:"
                        " --files file1 file2 file3")

    parser.add_argument("-s", "--start",
                        type=validate_date_string,
                        required=True, help="start time,"
                        " The timestamp from which to begin,"
                        " specified in a format of either:"
                        " \"%%Y-%%m-%%d %%H:%%M:%%S\""
                        " or \"%%Y-%%m-%%d\". The start time is inclusive."
                        " UTC is the only timezone supported and"
                        " will be used automatically")

    parser.add_argument("-e", "--end",
                        type=validate_date_string,
                        required=True, help="end time,"
                        " The upperbound of the time interval"
                        " of which we will consider,"
                        " specified in format of either:"
                        " \"%%Y-%%m-%%d %%H:%%M:%%S\", or"
                        " \"%%Y-%%m-%%d\". End time is exclusive."
                        " UTC is the only timezone supported and"
                        " will be used automatically")

    parser.add_argument("-k", "--kind",
                        required=False,
                        help="Specify any non-500 HTTP status"
                        " code to check. This utility defaults"
                        " to an HTTP 500 status code unless overridden"
                        " with this flag.")

    args = parser.parse_args()

    files = [file for file in args.files]

    if not args.kind:
        parse_log(files, args.start, args.end)
    else:
        args.kind = int(args.kind)
        parse_log(files, args.start, args.end, kind=args.kind)
